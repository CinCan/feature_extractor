#docker run -v $(pwd):/data cincan/feature_extractor --injsonl /data/jsonl_input --path /data/
#cincan run cincan/feature_extractor --injsonl jsonl_input --path ./

FROM python:3.6-slim-buster

LABEL MAINTAINER=cincan.io

COPY . /wp1/feature_extractor

RUN apt update && apt install -y \
	&& cd wp1/feature_extractor/ \
	&& pip install -r requirements.txt \
	&& adduser --shell /sbin/nologin --disabled-login --gecos "" appuser \
	&& chown -R appuser:appuser /wp1

USER appuser

WORKDIR /home/appuser

ENTRYPOINT ["/usr/local/bin/python","/wp1/feature_extractor/analyze_parallel.py","--confpath", "/wp1/feature_extractor"]
