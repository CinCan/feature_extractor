#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import os
import sys
import json
import subprocess
import argparse
import shutil
import webbrowser
import threading
import pandas as pd
import time
import ntpath
import string
import hashlib

from colorama import Fore, Style, init

from report import Report

lock = threading.Lock()
init()


class Config:

    def __init__(self, outpath, confpath):
        if (os.path.isdir(os.path.join(confpath+"/lib", "Cortex-Analyzers", "analyzers"))):
            lib_path = os.path.join(confpath+"/lib", "Cortex-Analyzers", "analyzers")
        else:
            print("path not found...")
            exit()

        self.cortex_analyzers_path = lib_path

        self.config_path = outpath+"/API.json"
        print("using configuration path: " + confpath + " and output: " + outpath)
        self.active_analyzers_config_path = outpath+"/active_analyzers"

        self.mispwarninglist_path = confpath #""
        self.analyzer_type_path = outpath+"/analyzer_type.json"

        self.init_config_paths()

        (self.config, self.active_analyzers_config) = self.reload_config()
        self.analyzer_type = self.save_analyzer_type_json()

        self.analyzer_configs = {}

    def init_config_paths(self):
        if not os.path.exists(self.config_path):
            try:
                with open(self.config_path, "w") as f:
                    f.write("{}")
            except:
                print("error1: could not open path...",self.config_path)
                exit()

    def reload_config(self):
        try:
            with open(self.config_path, "r") as f:
                config = json.loads(f.read())
        except:
            print("error2: could not open path...",self.config_path)
            exit()


        # Check all required services have API key
        for service in config:
            for required_field in config[service]:
                if len(config[service][required_field]) == 0:
                    print(Fore.RED + "{} missing from {}".format(required_field, service) + Style.RESET_ALL)

        # If active_analyzers file does not exist, create a sample file
        if not (os.path.isfile(self.active_analyzers_config_path)):
            try:
                with open(self.active_analyzers_config_path, "w") as f:
                    f.write("#AbuseIPDB/AbuseIPDB\n")
                    f.write("#Censys/Censys\n")
                    f.write("#GoogleSafebrowsing/GoogleSafebrowsing\n")
                    f.write("#MISPWarningLists2/MISPWarningLists2\n")
                    f.write("#PhishTank/PhishTank_CheckURL\n")
                    f.write("#DShield/DShield_lookup\n")
                    f.write("#GreyNoise/GreyNoise\n")
                    f.write("#OTXQuery/OTXQuery\n")
                    f.write("#Shodan/Shodan_DNSResolve\n")
                    f.write("#Shodan/Shodan_Host\n")
                    f.write("#Shodan/Shodan_Host_History\n")
                    f.write("#Shodan/Shodan_InfoDomain\n")
                    f.write("#Shodan/Shodan_ReverseDNS\n")
                    f.write("#Shodan/Shodan_Search\n")
                    f.write("#Threatcrowd/Threatcrowd\n")
                    f.write("#EmergingThreats/EmergingThreats_DomainInfo\n")
                    f.write("#VirusTotal/VirusTotal_GetReport\n")
                    f.write("#VirusTotal/VirusTotal_Scan\n")
                    f.write("#CinCanTools/CinCanTools\n")
                    f.close()
            except:
                print("error: could not create active_analyzers file",self.active_analyzers_config_path)
                exit()

        try:
            with open(self.active_analyzers_config_path, "r") as f:
                # Split file lines to list
                active_analyzers_config = f.read().split("\n")
                # Remove empty lines
                active_analyzers_config = list(filter(lambda line: len(line) > 0, active_analyzers_config))
                # Remove commented lines
                active_analyzers_config = list(filter(lambda analyzer: analyzer[0] != "#", active_analyzers_config))
        except:
            print("error: path not found for active_analyzers")
            exit()

        return (config, active_analyzers_config)

    def save_config(self):
        try:
            with open(self.config_path, "w") as f:
                f.write(json.dumps(self.config, indent=2))
        except:
            print("error3: could not open path...",self.config_path)
            exit()

    def get_required_fields(self, directory, service):
        service_config_path = os.path.join(self.cortex_analyzers_path, directory, service + ".json")
        try:
            with open(service_config_path, "r") as f:
                analyzer_config = json.loads(f.read())
                self.analyzer_configs[service] = analyzer_config
        except:
            print("error4: could not open path...",service_config_path)
            exit()

        configuration_items = analyzer_config.get("configurationItems", [])

        required_fields = []
        for item in configuration_items:
            if item["required"] == True:
                required_fields.append(item["name"])

        return required_fields

    def update(self):
        directories = []
        for analyzer in self.active_analyzers_config:
            directory, service = analyzer.split("/", 1)
            directories.append(directory)
            required_fields = self.get_required_fields(directory, service)

            if len(required_fields) == 0:
                self.config[directory] = {}

            for field in required_fields:
                if directory not in self.config:
                    self.config[directory] = {}

                if field not in self.config[directory]:
                    self.config[directory][field] = ""

        # Find which directories are not in active_analyzers
        directories_to_remove = []
        for directory in self.config:
            if directory not in directories:
                directories_to_remove.append(directory)

        # Remove directories that are not in active_analyzers
        for directory in directories_to_remove:
            self.config.pop(directory, None)

        self.save_config()
        print("Activated analyzers in current {}:".format(self.config_path))
        for item in self.config:
            print(item)

    def save_analyzer_type_json(self):
        analyzer_type = {"types": {}}

        for analyzer in self.active_analyzers_config:
            analyzer_path = os.path.join(self.cortex_analyzers_path, analyzer + ".json")

            try:
                with open(analyzer_path, "r") as f:
                    datatypes = json.loads(f.read())["dataTypeList"]
            except:
                print("error5: could not open path...",analyzer_path)
                exit()

            for datatype in datatypes:
                if datatype not in analyzer_type["types"]:
                    analyzer_type["types"][datatype] = []

                analyzer_type["types"][datatype].append({"path": analyzer})

        try:
            with open(self.analyzer_type_path, "w") as f:
                analyzer_type_json = json.dumps(analyzer_type, indent=2)
                f.write(analyzer_type_json)
        except:
            print("error6: could not open path...",self.analyzer_type_path)
            exit()

        return analyzer_type


class Analyzer:

    def __init__(self, path, confpath):
        self.timeout = 5
        self.config = Config(path, confpath)

        self.report = Report(path, confpath)

        self.output_files_path = path+"/output/"
        self.sample_files_path = path+"/samples"

        self.output = {}
        self.processed_samples = []

        self.init_paths()

    def init_paths(self):
        if os.path.exists(self.output_files_path):
            shutil.rmtree(self.output_files_path)

        if os.path.exists(self.sample_files_path):
            shutil.rmtree(self.sample_files_path)

        os.mkdir(self.output_files_path)
        os.mkdir(self.sample_files_path)

    def datatype_exists(self, datatype):
        return datatype in self.config.analyzer_type["types"]

    def create_sample(self, datatype, data, analyzer, service, path):
        sample = {"data": data, "dataType": datatype, "tlp": 0, "config":
                        {"max_tlp": 3, "check_tlp": "false"}
                    }

        for required_field in self.config.config[analyzer]:
            sample["config"][required_field] = self.config.config[analyzer][required_field]

        try:
            sample["config"]["service"] = self.config.analyzer_configs[service]["config"]["service"]
        except:
            print(Fore.YELLOW + "No ['config']['service'] for {}".format(analyzer) + Style.RESET_ALL)

        sample_path = os.path.join(self.sample_files_path, "sample_{}_{}.json".format(path, service))

        try:
            with open(sample_path, "w") as f:
                f.write(json.dumps(sample, indent=2))
        except:
            print("error7: could not open path...", sample_path)
            exit()

        return sample_path

    def run_analyzer(self, directory, service, sample_path, confpath, path):
        output_path = os.path.join(confpath,"output", "output_{}_{}.json".format(path, service))
        print("output_path",output_path,"service",service,"path",path)
        command = self.config.analyzer_configs[service]["command"]
        command_path = os.path.join(self.config.cortex_analyzers_path, command)

        try:
            with open(output_path, "w") as outf:
                try:
                    with open(sample_path, "r") as inpf:
                        proc = subprocess.Popen(["timeout", str(self.timeout),
                                                    "python3", command_path], stdout=outf, stdin=inpf)
                        proc.wait()
                except:
                    print("error8: could not open path...",sample_path)
                    exit()
        except:
            print("error9: could not open path...",output_path)
            exit()

        return output_path

    def process_sample(self, sample, confpath):
        with lock:
            if sample in self.processed_samples:
                return

        # restrict file name length, use hash instead
        datatype, data = sample.split(":", 1)
        hash = hashlib.md5(str(data).encode('utf-8'))
        path = datatype + str(hash.hexdigest())
        path = path.replace(os.path.sep, "?")

        if not self.datatype_exists(datatype):
            return

        for item in self.config.analyzer_type["types"][datatype]:
            directory, service = item["path"].split("/")
            sample_path = self.create_sample(datatype, data, directory, service, path)
            output_path = self.run_analyzer(directory, service, sample_path, confpath, path)

            with lock:
                self.read_output(datatype, data, output_path, confpath)

        with lock:
            self.processed_samples.append(sample)

    def read_output(self, datatype, data, filepath, confpath):
        if data not in self.output:
            self.output[data] = {"data": [], "titles": [], "html": []}

        try:
            with open(filepath, "r") as f:
                filedata = f.read()
                if len(filedata) != 0:
                    jsondata = json.loads(filedata)
                else:
                    return
        except:
            print("error10: could not open path...",self.config_path)
            exit()

        filename = ntpath.basename(filepath)
        service = filename.split("_", 2)[-1].split(".")[0]

        self.output[data]["data"].append(jsondata)
        self.output[data]["titles"].append(service)
        self.output[data]["html"].append(self.report.generate_parsed_html(service, datatype, filepath, confpath))

        return self.output


class ThreadManager:

    def __init__(self):
        self.threads = []

    def join_all(self):
        for thread in self.threads:
            thread.join()
            print("{} finished.".format(thread.getName()))

    def start_thread(self, target, args):
        t = threading.Thread(target=target, args=args)
        t.start()
        self.threads.append(t)


class IOCPreader:

    def __init__(self, path):
        self.path = path
        self.datatype_mappings = {  "host": "domain",
                                    "filename": "file",
                                    "ip": "ip",
                                    "sha256": "hash",
                                    "sha1": "hash",
                                    "md5": "hash",
                                    "url": "url",
                                    "email": "mail",
                                    }

        self.default_columns = ["file", "page", "datatype", "data"]
        self.data = {}

    def read(self):
        # Header not included in IOCP file so, assign columns manually.
        df = pd.read_csv(self.path, header=None)
        df.columns = self.default_columns

        # Change datatypes according to Cortex analyzers.
        df["datatype"] = df["datatype"].apply(lambda datatype: self.datatype_mappings[datatype.lower()])

        self.data = (df["datatype"].values, df["data"].values)
        return self.data

class ArgsHandler:

    def __init__(self, use_threading=True):
        self.use_threading = use_threading
        self.tm = ThreadManager()
        self.parser, self.args = self.init_parser()

        # Set output path, if defined
        if self.args["path"]:
           path = self.args["path"]
        else:
           path = "./"

        # Set configuration path, if defined
        if self.args["confpath"]:
            confpath = self.args["confpath"]
        else:
            confpath = "./"


        self.a = Analyzer(path, confpath)

        self.a.config.update()
        self.allow_only_iocp()
        self.handle_args()

    def init_parser(self):
        parser = argparse.ArgumentParser(description="CLI for Cortex Analyzers")
        parser.add_argument("--infile", help="File to read input from. Each line must be in format datatype:data. For example: ip:8.8.8.8")
        parser.add_argument("--injsonl", help="File to read input from as JSONL format. (compatible with ioc_strings tool)")
        parser.add_argument("--file", help="Filepath to process")
        parser.add_argument("--ip", help="IP address to process")
        parser.add_argument("--domain", help="Domain address to process")
        parser.add_argument("--fqdn", help="fqdn to process")
        parser.add_argument("--hash", help="hash to process")
        parser.add_argument("--mail", help="mail to process")
        parser.add_argument("--other", help="other to process")
        parser.add_argument("--url", help="url to process")
        parser.add_argument("--iocp", help="Path to ioc_parser file (https://github.com/armbues/ioc_parser)")
        parser.add_argument("--update", action="store_true", help="Update config")
        parser.add_argument("--browser", action="store_true", help="Show results in a browser window")
        parser.add_argument("--confpath", help="Path for configuration files (templates / lib)")
        parser.add_argument("--path", help="Set path for output files")
        args = vars(parser.parse_args())
        return parser, args

    def allow_only_iocp(self):
        for arg in self.args:
            if arg == "iocp":
                continue

            if self.args[arg] and self.args["iocp"]:
                print(Fore.RED + "When --iocp is used, no other arguments are allowed." + Style.RESET_ALL)
                self.parser.print_help()
                exit()

    def process_datatype(self, datatype, confpath):
        print("Processing: {}".format(datatype))

        # Don't fail if argument has no value
        try:
            self.args[datatype] = self.args[datatype].split(",")
            self.args[datatype] = list(map(lambda data: "{}:{}".format(datatype, data), self.args[datatype]))
            for sample in self.args[datatype]:
                if self.use_threading:
                    self.tm.start_thread(self.a.process_sample, (sample,confpath,))
                else:
                    self.a.process_sample(sample, confpath)
        except:
            pass

    def append_args_by_datatype(self, datatype, data):
        if not self.args[datatype]:
            self.args[datatype] = []

        self.args[datatype].append(data)

    def format_args_by_datatypes(self, datatypes):
        for datatype in set(datatypes):
            self.args[datatype] = ",".join(self.args[datatype])

    def handle_args(self):
        # Set path for configuration files
        if self.args["path"]:
            path = self.args["path"]
        else:
            path = "./"

        if self.args["confpath"]:
            confpath = self.args["confpath"]
        else:
            confpath = "./"

        # If no arguments set, print out help
        if len(set(self.args.values()).difference(['None','False'])) < 3:
            self.parser.print_help()
            exit()

        if self.args["update"]:
            print("Updating config...")
            self.a.config.update()
            exit()

        if self.args["iocp"]:
            print("Parsing IOCP...")
            iocpr = IOCPreader(self.args["iocp"])
            datatypes, datas = iocpr.read()
            for datatype, data in zip(datatypes, datas):
                self.append_args_by_datatype(datatype, data)

            self.format_args_by_datatypes(datatypes)

        if self.args["infile"]:
            print("Parsing {}...".format(self.args["infile"]))
            datatypes = []

            try:
                with open(self.args["infile"], "r") as f:
                    for line in f.readlines():
                        line = line.strip()
                        datatype, data = line.split(":", 1)
                        self.append_args_by_datatype(datatype, data)
                        datatypes.append(datatype)
            except:
                print("error11: could not open path...", self.args["infile"])
                exit()

            self.format_args_by_datatypes(datatypes)

        if self.args["injsonl"]:
            print("Parsing {}...".format(self.args["injsonl"]))
            datatypes = []
            with open(self.args["injsonl"], "r") as f:
                for line in f.readlines():
                    line = line.strip()
                    json_line = json.loads(line)
                    datatype = json_line["ioc_types"][0]
                    data = json_line["ioc"]
                    self.append_args_by_datatype(datatype, data)
                    datatypes.append(datatype)

            self.format_args_by_datatypes(datatypes)

        for datatype in self.args:
            if self.args[datatype]:
                self.process_datatype(datatype, confpath)

        self.tm.join_all()
        print("Generating report...")
        self.a.report.save_json_output(self.a.output, path)
        self.a.report.generate_report2(self.a.output, path, confpath)

        if self.args["browser"]:
            webbrowser.open('file://' + os.path.realpath("features.html"))
        else:
            print("features.html created")

        print(Fore.GREEN + "Done." + Style.RESET_ALL)

if __name__ == "__main__":
    ArgsHandler()
