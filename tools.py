#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import os, sys, json, codecs, logging, subprocess, re, csv, pickle

from lib.gibberish import gib_detect_train

import validators

from pyxdameraulevenshtein import normalized_damerau_levenshtein_distance_ndarray
import numpy as np

def intersect():
    with open("majestic_10k.csv", 'r') as file:
        csv_reader = csv.reader(file, delimiter=',')
        a = [{k: v for k, v in row.items()} for row in csv.DictReader(file, skipinitialspace=True)]
        tophosts = [d['Domain'] for d in a]

        tophosts= sorted(tophosts)
        with open("majestic_10ksorted.txt", 'w') as f:
            for host in tophosts:
                f.write(host + '\n')    

        #top10k= a[:10000]
    with open("2017.csv", 'r') as file1:
        b = [{k: v for k, v in row.items()} for row in csv.DictReader(file1, skipinitialspace=True)]
        #print(b)
        #print(top10k)
        #for i, row in enumerate(a):
         #   if row["Domain"] != row["IDN_Domain"]:
                #print("Domain: " + row["Domain"] +  "IDN_Domain: " + row["IDN_Domain"])
          #      print("rank: " + row["GlobalRank"] + "IDN_Domain: " + row["IDN_Domain"])
          #  if i % 10000 ==0:
           #     print(i)
        c = list(filter(lambda e: e['type'] == 'Host', b))
        #print(c)
        badhosts = []
        for entry in c:
            badhosts.append(entry["ioc"])
            badhosts=sorted(list(dict.fromkeys(badhosts)))
        #print(badhosts)

        print(list(set(tophosts).intersection(badhosts)))

def load_badhosts():
    b = []
    with open("2017.csv", 'r') as file1:
        b = [{k: v for k, v in row.items()} for row in csv.DictReader(file1, skipinitialspace=True)]
        c = list(filter(lambda e: e['type'] == 'Host', b))  
        badhosts = sorted([d['ioc'] for d in c])
        badhosts=list(dict.fromkeys(badhosts))
        return badhosts

def load_tophosts(n):
    a=[]
    with open("majestic_million.csv", 'r') as file:
        csv_reader = csv.reader(file, delimiter=',')
        a = [{k: v for k, v in row.items()} for row in csv.DictReader(file, skipinitialspace=True)]
        tophosts = [d['Domain'] for d in a]
        top= tophosts[:n]    
        return top

def dots(input,n):
    return [x for x in input if x.count('.')>=n]


def normalized_dl(top,badhosts,low,high):
    top = np.array(top)


    for host in badhosts:
        #print(host)
        dist = normalized_damerau_levenshtein_distance_ndarray(host, top)
        for i, d in enumerate(dist):
            if(d <=high and d >= low):
                print("host: "+ host + " compared to: " + top[i])

def contained_in(top,badhosts):
    for host in badhosts:
        #print(host)
        for i, d in enumerate(top):
            if(d in host):
                print(top[i]+ " in: " + host)   

def detect_generated_url(text):
    model_data = pickle.load(open('./lib/gibberish/gib_model.pki', 'rb'))

    model_mat = model_data['mat']
    threshold = model_data['thresh']

    result=gib_detect_train.avg_transition_prob(text, model_mat) > threshold
    if result==True:
        print(text + ": "+ str(result))

def validate_domain(domain):
    try:
        print(domain)
        validators.domain(domain)
    except ValidationFailure as v:
        print(v)

# def contains_known_domain(domain):
#     top=load_tophosts(100)
#     for t in top:


#return array 
def common_domains(top,length):
    fqdn = []
    top= load_tophosts(top)
    with open("domains.txt", 'w', encoding='utf-8') as f:
        
        for t in top:
            f.write(t+'\n')
            if t.count('.') ==1:
                s = t.split('.')
                fqdn.append(dict(tld=s[1], domain=s[0]))
                if(s[0] == "www"):
                    print(fqdn[-1])
                #print(fqdn[-1])


            elif any(t.endswith(x) for x in ['.co.uk', '.com.hk', '.gov.cn', '.co.in', '.co.jp', '.com.br', '.com.au',
                                    '.gov.au', '.org.uk', '.com.cn', '.edu.au', '.ac.uk', '.com.es', '.net.au',
                                    '.ne.jp']):
                s =list(reversed(t.rsplit('.',3)))
                if len(s) == 4:
                    fqdn.append(dict(tld=s[0], sld=s[1], domain=s[2], rest=s[3] ))

                else:
                    fqdn.append(dict(tld=s[0], sld=s[1], domain=s[2]))

                
                #print(t)
            else:
                
                s =list(reversed(t.rsplit('.',2)))
                fqdn.append(dict(tld=s[0], domain=s[1], rest=s[2]))
                print(fqdn[-1])
        domains=[]
        for domain_name in fqdn:
            domains.append(domain_name["domain"])
        domains = sorted(set(domains))            
        domains_len= []
        for d in domains:

            if len(d)>=length:
                domains_len.append(d)
        #print(domains_len5)

    return domains_len
                
    badhosts=load_badhosts()
    for host in badhosts:
        if any(x in host for x in domains_len5):
            print(host)


def main():

    #domains = sorted(list(set(domains)))
   # for d in domains:
     #   print(d)
    



    #for t in top:
     #   t=t.rsplit('.',1)[0]
      #  print(t)

    intersect()
    return
    
    domains = common_domains(1000,5)
    badhosts=load_badhosts()    

    #top = load_tophosts(1000)

    #for host in badhosts:
        #validate_domain(host)
        #contains_known_domain(host)


    #normalized_dl(top,badhosts,0,0.01)
    contained_in(domains,badhosts)


    #output = dots(badhosts,4)
    #for host in output:
    #    print(host)
   # for host in badhosts:
        #print(host)
    #    domain = host.rsplit('.', 1)[0]
     #   if '.' in domain:
            #print(domain)
      #      domain = domain.rsplit('.', 1)[1]
       # detect_generated_url(domain)

if __name__ == '__main__':
    main()