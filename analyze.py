#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import os, sys, json, codecs, logging, subprocess, re, csv, webbrowser

from io import open
#from abuseipdb import AbuseIPDBAnalyzer
from css_html_js_minify import js_minify,css_minify
from collections import Counter
from jsonpath_ng import parse, jsonpath #JSONPath

from json2html import *


import validators


from jinja2 import Environment, PackageLoader, select_autoescape, FileSystemLoader

if sys.version_info >= (3, 0):
    from io import StringIO
    unicode = str
    import importlib
else:
    from StringIO import StringIO
    import imp

sample = ""
cortex_analyzers_path="./lib/Cortex-Analyzers/analyzers/" #relative to this file
jsonviewer_path = "./lib/json-viewer/"
config_file = "API.json"
active_analyzers_file = "active_analyzers"
default_mispwarninglist_path = "/path/to/misp-warninglists/"


#writes JSON containing paths to analyzers, sorted by accepted input data types
def sort_analyzers_by_type():
    a = dict(types=dict())
    #a["types"]["ip"] = []
    #a["types"]["ip"].append(dict(path="da"))
    types_set = set();
    #input_json = sys.argv[1]
    with open(active_analyzers_file) as analyzers:
        temp = analyzers.read().splitlines() #strip \n 's
        for line in temp:
            if line.startswith('#'):
                continue
       #     x =line.split("/", 1)
            with open(cortex_analyzers_path + line + ".json") as data_file:
                data = json.load(data_file)
            for type in data["dataTypeList"]:
                if type not in types_set:
                    a["types"][type] = []            
                    types_set.add(type)
                a["types"][type].append(dict(path=line))
    a = json.dumps(a, indent=2, sort_keys=True)
    with open('analyzer_type.json', 'w', encoding='utf-8') as outfile:  
        outfile.write(unicode(a))


def create_sample(type, data, analyser, service):
    sample = dict(data=data, dataType=type, tlp=0, config=dict(max_tlp=3, check_tlp="false"))
    with open(config_file) as json_file:
        api_keys = json.load(json_file)
        
        if analyser in api_keys:    
            for a in api_keys[analyser]:
                sample["config"][a] = api_keys[analyser][a]

    with open(cortex_analyzers_path + analyser+'/'+service + ".json") as data_file:
        data = json.load(data_file)

    if "config" in data:
        if "service" in data["config"]:
            sample["config"]["service"] = data["config"]["service"]

    if not os.path.exists('samples'):
        os.makedirs('samples')
    
    sample = json.dumps(sample)
    with open("samples/sample_"+service+".json", 'w', encoding='utf-8') as outfile:
        outfile.write(unicode(sample))



def update_configuration():
    config = None
    
    try:
        with open(config_file) as conf_file:
            config = json.load(conf_file)
    except (ValueError, FileNotFoundError):
        config = dict()
    with open(active_analyzers_file) as analyzers:
        temp = analyzers.read().splitlines() #strip \n 's
        for line in temp:
            if line.startswith('#'):
                continue
            folder,service = line.split("/", 1)
            required_fields = get_required_fields(folder,service)
            
            if len(required_fields) > 0:
                if folder not in config:
                    print(folder)
                    config[folder] = dict()
                    for i in required_fields:

                        config[folder][i] = ""
                        if (folder == "MISPWarningLists" or folder == "MISPWarningLists2") and i == "path":
                            config[folder][i] = os.path.dirname(os.path.realpath(__file__)) + '/lib/misp-warninglists'
                else:
                    for i in required_fields:
                        if i not in config[folder]:
                            config[folder][i] = ""
                            if (folder == "MISPWarningLists" or folder == "MISPWarningLists2") and i == "path":
                                config[folder][i] = os.path.dirname(os.path.realpath(__file__)) + '/lib/misp-warninglists'
                        elif (folder == "MISPWarningLists" or folder == "MISPWarningLists2") and i == "path" and config[folder][i] == "":
                            config[folder][i] = os.path.dirname(os.path.realpath(__file__)) + '/lib/misp-warninglists'

    with open(config_file, 'w', encoding='utf-8') as outfile:
        conf = json.dumps(config, indent=4)
        outfile.write(unicode(conf))                            
        
        #conf_file.seek(0)
        #conf_file.write(conf)

            



def get_required_fields(folder,service):
    with open(cortex_analyzers_path + folder+'/'+service + ".json") as file:
        analyser_conf = json.load(file)
        required_fields =[]
        if "configurationItems" not in analyser_conf:
            return []
        for c in analyser_conf["configurationItems"]:
            if c["required"] == True:
                required_fields.append(c["name"])
        return required_fields

def check_required(folder, service):
    missing_fields = []
  #  print("folder:" + folder + " service:" + service)
    required_fields = get_required_fields(folder, service)
       # print(required_fields)
    with open(config_file) as conf_file:
        conf = json.load(conf_file)
        for f in required_fields:
            #print(f)
            #print(conf[folder][f])
            if f not in conf[folder]:
                missing_fields.append(f) 
            elif len(conf[folder][f]) ==0:
                missing_fields.append(f) 
            elif folder == "MISPWarningLists" and conf[folder][f] == default_mispwarninglist_path:
                missing_fields.append(f)
    if len(missing_fields)>0:
        return missing_fields
    return True


def run_analyzer(folder, service):
    status =check_required(folder,service)
    if status == True:
        #print("folder:" + folder + " service:" + service)
        input_json = "samples/sample_"+service+".json"
        if not os.path.exists('output'):
            os.makedirs('output')
        output = "output/output_"+service+".json"


        with open(cortex_analyzers_path + folder+'/'+service + ".json") as data_file:
            data = json.load(data_file)
        command =data["command"]
        
        if os.name =='nt':
            proc = subprocess.Popen("python analyzers/"+command+" .\\analyzers\\" + folder, shell=True)
            proc.wait()
        if os.name == 'posix':
            with open(output,'w') as ouf:
                with open(input_json,'r') as inf:
                    proc = subprocess.Popen(['python3',cortex_analyzers_path +command],stdout=ouf,stdin=inf)
                    proc.wait()
    else:
        fields = ', '.join([str(x) for x in status])
        print(service + ": Missing following fields from " +config_file +": " + fields +". Ignoring")
 
def pretty_output():
    for filename in os.listdir("./output"):
        #print filename
        if filename.endswith(".json"): 
            with open("./output/"+filename, 'r') as file:
                try:
                    data = json.load(file)
                    sample = json.dumps(data, indent=2, sort_keys=True)
                    with open("./output/"+filename, 'w', encoding='utf-8') as outfile:
                        outfile.write(unicode(sample))    
                except ValueError:
                    print("Error: " +  filename + " is not a valid JSON file")

 


def copy_input_files(folder, service):
    if not os.path.exists(".\\Cortex-Analyzers\\analyzers\\"+folder+"\\input"):
        os.makedirs(".\\Cortex-Analyzers\\analyzers\\"+folder+"\\input")
    if os.path.isfile(".\\Cortex-Analyzers\\analyzers\\"+folder+"\\input\\input.json"):
        os.remove(".\\Cortex-Analyzers\\analyzers\\"+folder+"\\input\\input.json")
    os.rename(".\\samples\\sample_"+service+".json", ".\\Cortex-Analyzers\\analyzers\\"+folder+"\\input\\input.json")
def clean_input_files(folder, service):
    if os.path.isfile(".\\Cortex-Analyzers\\analyzers\\"+folder+"\\input\\input.json"):
        os.remove(".\\Cortex-Analyzers\\analyzers\\"+folder+"\\input\\input.json")
def copy_output_files(folder, service):
    if not os.path.exists(".\\Cortex-Analyzers\\analyzers\\"+folder+"\\input"):
        os.makedirs(".\\Cortex-Analyzers\\analyzers\\"+folder+"\\input")
    if os.path.isfile(".\\output\\output_"+service+".json"):
        os.remove(".\\output\\output_"+service+".json")
    if os.path.isfile(".\\Cortex-Analyzers\\analyzers\\"+folder+"\\output\\output.json"):
        os.rename(".\\Cortex-Analyzers\\analyzers\\"+folder+"\\output\\output.json", ".\\output\\output_"+service+".json")
def clean_output_files():
    if not os.path.exists("./output"):
        return
    for filename in os.listdir("./output"):
        #print filename
        if filename.endswith(".json"): 
            os.remove("./output/"+filename)


     


def generate_report2(data):
    env = Environment(
        loader = FileSystemLoader('./templates')#,
       # autoescape=select_autoescape(['html', 'xml'])
    )
    template = env.get_template('features_template.html')
    with open(jsonviewer_path+ '/src/json-viewer.js') as jsonviewer_file:
        jsonviewer = js_minify(jsonviewer_file.read())
        jsonviewer = jsonviewer.replace(';else','else') #hack to fix bug in js_minify
    with open(jsonviewer_path+ '/src/json-viewer.css') as jsonviewercss_file:
        jsonviewercss = css_minify(jsonviewercss_file.read())
    with open('templates/style.css') as stylecss_file:
        stylecss = css_minify(stylecss_file.read())
    t = template.render(data=data, jsonviewer=jsonviewer, jsonviewercss=jsonviewercss, style=stylecss)
    with open('features.html', 'w', encoding='utf-8') as outfile:  
        outfile.write(unicode(t))    

def generate_data(type):
    data = []
    titles = []
    html = []
    for filename in sorted(os.listdir("./output")):
        with open("output/"+ filename) as json_file:
            try:
                file_data = json.load(json_file)
            except ValueError:
                file_data = "{}"
        #file_data =  json.dumps(file_data)
        data.append(file_data)
        service = filename.split('_',1)[1].rsplit('.',1)[0]
        titles.append(service)
        html.append(generate_parsed_html(service, type))
    #print(html)
    return data, titles, html


def generate_parsed_html(service, type):
    output=""
    #print(service + " " + type)
    env = Environment(
            loader = FileSystemLoader('./templates')#,
           # autoescape=select_autoescape(['html', 'xml'])
        )
    template = env.get_template('field.html')   
    with open('Track.json') as f:
        t = json.load(f)
    for a in t["tracked"]:
        if a['service'] == service and os.path.exists('output/output_' + a["service"] + '.json'):

            with open('output/output_' + a["service"] + '.json') as f1:
                data = json.load(f1)
            for e in a["fields"]:
                if type not in e["datatypes"]:
                    continue
                expr = parse(e["expression"])
                #try:
                 #   print([str(match.path) for match in expr.find(data)])
                    #print([match.full_path for match in expr.find(data)])
                    #print([match for match in expr.find(data)])
                    #print([match.field for match in expr.find(data)])
                    #print([match.key for match in expr.find(data)])
                #except AttributeError as ee:
                 #   print(ee)
                
                if len([match.value for match in expr.find(data)]) == 0:
                    continue

                if e.get("type") == "multiset":
                    test= Counter([match.value for match in expr.find(data)])
                    content = e["name"] + ": "

                    for key, count in test.most_common():
                        content += key+ ': ' +str(count) +', '
                    if len(content) > 0:
                        content = content.rsplit(',',1)[0]
                
                elif e.get("type") == "object":
                    content = "<div>" + e["name"]+ ":</div>".strip()
                    #print(str([match.value for match in expr.find(data)]))
                    content += json2html.convert(json=str((json.dumps([match.value for match in expr.find(data)])))[1:-1], table_attributes="border=\"0\"")

                elif e.get("type") == "object-array":
                    
                    fields =e["expression"].rsplit('[',1)[1].strip(']').split(',')
                    data1 = [match.value for match in expr.find(data)]

                    #generate array of dictionaries
                    a = []
                    b = {}
                    for i in range(len(data1)):
                        if i % len(fields) == 0:
                            if i > 0:
                                a.append(b)     
                            b ={}
                        b[fields[i%len(fields)]]=data1[i]             
                    a.append(b)           
                    

                    content = e["name"] + ": "
                    content += json2html.convert(json=str((json.dumps(a))), table_attributes="border=\"0\"")
                elif e.get("type") == "subobject":
                    fields = [str(match.path) for match in expr.find(data)]
                    #fields =e["expression"].rsplit('[',1)[1].strip(']').split(',')

                    data2 = [match.value for match in expr.find(data)] 
                    #print(fields)
                    #print(data2)                       
                    a = {}
                    for i in range(len(data2)):
                        a[fields[i]]=data2[i]       
                    content = e["name"] + ": "
                    content += json2html.convert(json=str(json.dumps(a)), table_attributes="border=\"0\"")
                else:
                    content = e["name"] + ": "
                    returned = [match.value for match in expr.find(data)]
                    a = []
                    #print(returned)
                    myString = ', '.join(map(str, returned)) 
                    #print(myString)
                    for r in returned:
                        try:
                            #print(r)
                            r =r.strip('\'')
                        except AttributeError:
                            pass

                        a.append(r)
                    #content += str((json.dumps(a)))[1:-1]
                    content += myString
            
                field = "class"
                temp = template.render(class_str=field, content=content)
                output += temp
    #print(output)
    return output

def return_multiset():
    return ""


def detect_sample_type(sample):
    if validators.ip_address.ipv4(sample):
        return "ip"
    if validators.domain(sample):
        return "domain"
    if validators.url(sample):
        return "url"
    if validators.email(sample):
        return "mail"
    else:
        return "unknown type"


def process_sample(input_data, output_data):
    type, sample = input_data.rstrip().split(":", 1)
    clean_output_files()
    with open("analyzer_type.json") as json_file:
        file_data = json.load(json_file)
    if type  not in file_data["types"]:
        return
    for d in file_data["types"][type]:
        x = d["path"].split("/", 1)
        folder=x[0] 
        name=x[1]
        create_sample(type, sample, folder, name)
        if os.name == 'nt':
            copy_input_files(folder, name)

        run_analyzer(folder, name)

        if os.name == 'nt':
            clean_input_files(folder,name)
            copy_output_files(folder, name)
    if sample not in output_data:
        output_data[sample] = dict()
    output_data[sample]["data"], output_data[sample]["titles"], output_data[sample]["html"] = generate_data(type)    

def print_help():
    print("CLI for Cortex Analyzers")

    print('Usage : ./analyze.py [-s] datatype:data/-f file/-i file')
    print(' ')
    print('Optional parameters:')
    print("  -s, --silent          don't open report at the end")
    print(' ')
    print('Positional parameters:')
    print("  datatype:data        run active analyzers for data. e.g. ip:8.8.8.8, domain:example.com, hash:e3b0c44298fc1c149afbf4c8996fb92427ae41e4649b934ca495991b7852b855")
    print("  -f, --file file_name  run active analyzers to entries in file file_name")
    print("  -i, --iocp file_name  run active analyzers to entries in file file_name in ioc_parser csv format")
    print("  -h, --help            display this help and exit")
    print("  -u, --update-config   update "+config_file +" by appending required fields for active analysers (user has to supply API keys manually)")



def read_iocparse_csv(file):
    line_format= "file,page,type,data"
    field_names = ["file", "page", "type", "data"]
    entries = []
    data = []
    sample_type = ""
    with open(file, mode='r') as f:
        csv_reader = csv.DictReader(f, fieldnames= field_names)
        for line in csv_reader:
            #print(line["type"] +':'+ line["data"])
            if line["type"] == "Host":
                sample_type = "domain"
            elif line["type"] == "Filename":
                sample_type = "file"
            elif line["type"] == "IP":
                sample_type = "ip"
            elif line["type"] == "SHA256" or line["type"] == "SHA1" or line["type"] == "MD5":
                sample_type = "hash"
            elif line["type"] == "URL":
                sample_type = "url"
            elif line["type"] == "Email":
                sample_type = "mail"
            else:
                continue
            entries.append(dict(data=line["data"], type=line["type"], sample_type=sample_type))
            data.append(sample_type +':'+ line["data"])
    return data
            
                       

def parse_arguments():
    open_report= True

    sample_types = ["domain", "file", "fqdn", "hash", "ip", "mail", "other", "url"]

    for i, arg in enumerate(sys.argv):
        if arg in ['-s', '--silent']:
            del sys.argv[i]
            open_report= False

    for i, arg in enumerate(sys.argv):

        if arg in ['-h', '--help']:
            print_help()
            exit()

        if arg in ['-u', '--update-config']:
            update_configuration()
            exit()

        if arg in ['--file', '-f'] and len(sys.argv) > i + 1:
            update_configuration()
            input_file = sys.argv[i+1]
            del sys.argv[i]
            del sys.argv[i]
            output_data = dict()
            sort_analyzers_by_type()
            with open(input_file, 'r') as file:
                for line in file:
                    print(line.strip())
                    process_sample(line, output_data)

            output_data = json.dumps(output_data)
            output_data = output_data.replace("<script", "script").replace("</script", "script")
            generate_report2(output_data) 
            if open_report:
                webbrowser.open('file://' + os.path.realpath("features.html"))            
            exit()

        if arg in ['--iocparser', '-i'] and len(sys.argv) > i+1:
            update_configuration()
            input_file = sys.argv[i+1]
            output_data = dict()
            sort_analyzers_by_type()
            data = read_iocparse_csv(input_file)
            for line in data:
                print(line)
                process_sample(line, output_data)

            output_data = json.dumps(output_data)
            output_data = output_data.replace("<script", "script").replace("</script", "script")
            generate_report2(output_data) 
            if open_report:
                webbrowser.open('file://' + os.path.realpath("features.html"))            
            exit()

        if len(sys.argv)>1 and re.search(":+", sys.argv[1]):
            update_configuration()
            sort_analyzers_by_type()
            output_data = dict()
            input_data = sys.argv[1] #e.g ip:253.222.11.33
                
            if sys.argv[1].split(':')[0] in sample_types:
                process_sample(input_data, output_data)
                output_data = json.dumps(output_data)
                output_data = output_data.replace("<script", "script").replace("</script", "script")
                generate_report2(output_data)
                if open_report:
                    webbrowser.open('file://' + os.path.realpath("features.html"))
            exit()

    if len(sys.argv)>1:
        print("Unknown command. Display help with parameter -h or --help")    
        exit()

    print_help()
   # for i, arg in enumerate(sys.argv):
    #    if arg in ['--iocparser', '-i'] and len(sys.argv) > i + 1:


def main():    
    parse_arguments()
    exit()
        # print(detect_sample_type("iltalehti.fi"))
        # print(detect_sample_type("http://www.iltalehti.fi"))
        # print(detect_sample_type("3.fi"))
        # print(detect_sample_type("32.33.22"))
        # print(detect_sample_type("333.233.22.22"))
if __name__ == '__main__':
    main()