import os
import json

from jinja2 import Environment, PackageLoader, select_autoescape, FileSystemLoader
from jsonpath_ng import parse, jsonpath
from json2html import *
from css_html_js_minify import js_minify, css_minify
from collections import Counter

unicode = str


class Report:

    def __init__(self, outpath, confpath):
        self.track_json_path = confpath+"/Track.json"
        self.jsonviewer_path = os.path.join(confpath+"/lib", "json-viewer")

        self.track = self.load_track_json()

    def load_track_json(self):
        with open(self.track_json_path, "r") as f:
            t = json.loads(f.read())
        return t


    def generate_parsed_html(self, service, type, outpath, confpath):
        output=""
        #print(service + " " + type)
        env = Environment(
                loader = FileSystemLoader(confpath+'/templates')#,
            # autoescape=select_autoescape(['html', 'xml'])
            )
        template = env.get_template('field.html')
        for a in self.track["tracked"]:
            if a['service'] == service and os.path.exists(outpath+'/output/output_' + a["service"] + '.json'):

                with open(outpath+'/output/output_' + a["service"] + '.json') as f1:
                    data = json.load(f1)
                for e in a["fields"]:
                    if type not in e["datatypes"]:
                        continue
                    expr = parse(e["expression"])
                    #try:
                    #   print([str(match.path) for match in expr.find(data)])
                        #print([match.full_path for match in expr.find(data)])
                        #print([match for match in expr.find(data)])
                        #print([match.field for match in expr.find(data)])
                        #print([match.key for match in expr.find(data)])
                    #except AttributeError as ee:
                    #   print(ee)
                    
                    if len([match.value for match in expr.find(data)]) == 0:
                        continue

                    if e.get("type") == "multiset":
                        test= Counter([match.value for match in expr.find(data)])
                        content = e["name"] + ": "

                        for key, count in test.most_common():
                            content += key+ ': ' +str(count) +', '
                        if len(content) > 0:
                            content = content.rsplit(',',1)[0]
                    
                    elif e.get("type") == "object":
                        content = "<div>" + e["name"]+ ":</div>".strip()
                        #print(str([match.value for match in expr.find(data)]))
                        content += json2html.convert(json=str((json.dumps([match.value for match in expr.find(data)])))[1:-1], table_attributes="border=\"0\"")

                    elif e.get("type") == "object-array":
                        
                        fields =e["expression"].rsplit('[',1)[1].strip(']').split(',')
                        data1 = [match.value for match in expr.find(data)]

                        #generate array of dictionaries
                        a = []
                        b = {}
                        for i in range(len(data1)):
                            if i % len(fields) == 0:
                                if i > 0:
                                    a.append(b)     
                                b ={}
                            b[fields[i%len(fields)]]=data1[i]             
                        a.append(b)           
                        

                        content = e["name"] + ": "
                        content += json2html.convert(json=str((json.dumps(a))), table_attributes="border=\"0\"")
                    elif e.get("type") == "subobject":
                        fields = [str(match.path) for match in expr.find(data)]
                        #fields =e["expression"].rsplit('[',1)[1].strip(']').split(',')

                        data2 = [match.value for match in expr.find(data)] 
                        #print(fields)
                        #print(data2)                       
                        a = {}
                        for i in range(len(data2)):
                            a[fields[i]]=data2[i]       
                        content = e["name"] + ": "
                        content += json2html.convert(json=str(json.dumps(a)), table_attributes="border=\"0\"")
                    else:
                        content = e["name"] + ": "
                        returned = [match.value for match in expr.find(data)]
                        a = []
                        #print(returned)
                        myString = ', '.join(map(str, returned)) 
                        #print(myString)
                        for r in returned:
                            try:
                                #print(r)
                                r =r.strip('\'')
                            except AttributeError:
                                pass

                            a.append(r)
                        #content += str((json.dumps(a)))[1:-1]
                        content += myString

                    field = "class"
                    temp = template.render(class_str=field, content=content)
                    output += temp
        #print(output)
        return output

    def generate_report2(self, data, outpath, confpath):
        if type(data) == dict:
            data = json.dumps(data)
            data = data.replace("<script", "script").replace("</script", "script")
        env = Environment(
            loader = FileSystemLoader(confpath+'/templates')#,
        # autoescape=select_autoescape(['html', 'xml'])
        )
        template = env.get_template('features_template.html')
        with open(self.jsonviewer_path+ '/src/json-viewer.js') as jsonviewer_file:
            jsonviewer = js_minify(jsonviewer_file.read())
            jsonviewer = jsonviewer.replace(';else','else') #hack to fix bug in js_minify
        with open(self.jsonviewer_path+ '/src/json-viewer.css') as jsonviewercss_file:
            jsonviewercss = css_minify(jsonviewercss_file.read())
        with open(confpath+'/templates/style.css') as stylecss_file:
            stylecss = css_minify(stylecss_file.read())
        t = template.render(data=data, jsonviewer=jsonviewer, jsonviewercss=jsonviewercss, style=stylecss)
        features_html = str(outpath)+"/features.html"
        with open(features_html, 'w', encoding='utf-8') as outfile:
            outfile.write(unicode(t))

    def save_json_output(self, data, outpath):
        json_output = {}

        for key in data.keys():
            json_output[key] = {}
            for jsondata, service in zip(data[key]["data"], data[key]["titles"]):
                json_output[key][service] = jsondata

        with open(outpath+"/output.json", "w") as f:
            f.write(json.dumps(json_output, indent=2))
