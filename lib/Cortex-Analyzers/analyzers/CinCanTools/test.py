#!/usr/bin/env python
# encoding: utf-8

import requests, json, os
from cortexutils.analyzer import Analyzer


class CinCan(Analyzer):
    URI = "https://www.threatcrowd.org/searchApi/v2"

    def summary(self, raw):
        taxonomies = []

        level = None
        value = None

        if 'results' in raw:
            r = raw.get('results')
            value = r
            if r == 1:
                level = "safe"
            elif r == 0:
                level = "suspicious"
            elif r == -1:
                level = "malicious"
            else:
                value = "unknown"
                level = "info"
        else:
            value = "None"
            level = "info"

        taxonomies.append(self.build_taxonomy(level, "Test", "votes", value))

        result = {"taxonomies": taxonomies}
        return result

    @staticmethod
    def top10k(sample):
        with open(os.path.dirname(os.path.abspath(__file__)) +"/majestic_10ksorted.txt") as f:
            data = [line.strip() for line in f]
            if sample in data:
                return True
            else:
                return False

    def run(self):
        Analyzer.run(self)
        #self.report('{"test": "' +self.get_data()+ '"}')

        d = dict(test="bla",test2="bla")
        #d =json.dumps(d)
        self.report({
            "sample": self.get_data(),
            "majestic_top10k": self.top10k(self.get_data()),
            "test": d
        })
        #if self.data_type == 'domain' or self.data_type == 'ip' or self.data_type == 'mail':
            #threatcrowd_data_type = self.data_type if self.data_type != 'mail' else 'email'
            #try:
                #response = requests.get("{}/{}/report/".format(self.URI, threatcrowd_data_type),
                                        #{threatcrowd_data_type: self.get_data()})
                #self.report(response.json())
            #except Exception as e:
                #self.unexpectedError(e)
        #else:
            #self.notSupported()


if __name__ == '__main__':
    CinCan().run()
